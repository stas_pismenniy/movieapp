import React, {FC, useEffect, useCallback} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {FlatList, Image, StyleSheet, Text, View, Linking, Alert, TouchableOpacity} from 'react-native'
import {Cast} from '../components/Cast'
import api from '../services/api'
import {useRoute} from '@react-navigation/native'
import {API_KEY, IMAGES_URI} from '@env'
import {RootState} from '../store/reducers'
import {getMovie} from '../store/actions'
export const PostScreen: FC<any> = () => {
  const route: any = useRoute()
  console.log('RouteParams', route.params)
  const dispatch = useDispatch()
  const postId = route.params.postId
  const movies = useSelector((state: RootState) => state.movies.allMovies)
  const movie = movies.find(p => p.id === postId)
  const video = movie && movie?.videos && movie?.videos[0].key
  const videoURL = `https://www.youtube.com/watch?v=${video}`
  const loadMovie = async () => {
    try {
      if (!movie?.cast || !movie?.videos) {
        const url = `movie/${postId}?api_key=${API_KEY}&language=en-US&append_to_response=videos,credits`
        const response = await api.get(url)
        console.log('resultOk', response)
        return await dispatch(getMovie(response.data, response.data.id))
      }
    }
    catch (e) {
      console.error('error', e)
    }
  }
  useEffect(() => {
    loadMovie()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps
  const handlePress = useCallback(async () => {
    // Checking if the link is supported for links with custom URL scheme.
    const supported = await Linking.canOpenURL(videoURL)
    if (supported) {
      // Opening the link with some app, if the URL scheme is "http" the web link should be opened
      // by some browser in the mobile
      await Linking.openURL(videoURL)
    } else {
      Alert.alert(`Don't know how to open this URL: ${videoURL}`)
    }
  }, [videoURL])
  console.log('moviePostId', movie)
  console.log('MovieCast', movie?.cast)
  return (
    <>
      <Image style={styles.image} source={{uri: `${IMAGES_URI}${movie?.backdrop_path}`, cache: 'force-cache'}}/>
      <View style={styles.container}>
        <View style={styles.overViewWrap}>
          <Text style={styles.overViewTitle}>Overview</Text>
          <Text style={styles.overView}>
            {movie?.overview}
          </Text>
        </View>
        <Text style={styles.overViewTitle}>Cast</Text>
        <FlatList
          style={styles.flatList}
          data={movie?.cast}
          keyExtractor={item => item.id.toString()}
          renderItem={({item}) => (
            <Cast title={item.character}/>
          )}
        />
        <Text style={styles.overViewTitle}>Video</Text>
        <TouchableOpacity onPress={handlePress}>
          <Text style={styles.video}>Watch Video</Text>
        </TouchableOpacity>
      </View>
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // alignItems: 'center',
    paddingHorizontal: 20,
    paddingBottom: 20
  },
  image: {
    width: '100%',
    height: 350
  },
  overViewWrap: {
    marginTop: 20
  },
  overView: {
    fontWeight: '400',
    fontSize: 16,
    marginBottom: 10
  },
  overViewTitle: {
    fontWeight: '600',
    fontSize: 19,
    paddingBottom: 10
  },
  flatList: {
    marginBottom: 10
  },
  video: {
    fontSize: 14,
    fontWeight: '400',
    color: 'red'
  }
})
