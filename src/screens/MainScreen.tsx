import React, {FC, useEffect} from 'react'
import {useNavigation} from '@react-navigation/native'
import {useDispatch, useSelector} from 'react-redux'
import type {RootState} from '../store/reducers'
import {getMovies} from '../store/actions/'
import {View, StyleSheet, FlatList} from 'react-native'
import {Post} from '../components/Post'

export const MainScreen: FC<any> = () => {
  // let [page, onChangePage] = useState<number>(1)
  // const [movies, onChangeMovies] = useState<Movie[] | null>(null)
  const navigation = useNavigation<any>()
  const dispatch = useDispatch()
  const movies = useSelector((state: RootState) => state.movies.allMovies)
  const page = useSelector((state: RootState) => state.movies.page)
  console.log('movies', movies)
  console.log('PAGE123', page)
  useEffect(() => {
    if (!movies.length) {
      dispatch(getMovies(page))
    }
  }, []) // eslint-disable-line react-hooks/exhaustive-deps
  console.log('movies', movies)
  // Open post
  const openPostHandler = (post: any) => {
    navigation.navigate('PostScreen', {postId: post.id, postTitle: post.title})
  }
  //Fetch More items
  const fetchMore = () => {
    dispatch(getMovies(page))
    console.log('page', page)
  }
  // Render
  return (
    <View style={styles.container}>
      <FlatList
        data={movies}
        initialNumToRender={10}
        onEndReached={fetchMore}
        onEndReachedThreshold={0.5}
        keyExtractor={item => item.id.toString()}
        renderItem={({item}) => <Post post={item} onOpen={openPostHandler}/>}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})
