import React, {FC} from 'react'
import { useNavigation } from '@react-navigation/native'
import { useSelector } from 'react-redux'
import type { RootState } from '../store/reducers'
import { View, StyleSheet, FlatList} from 'react-native'
import {Post} from '../components/Post'

export const Favorites: FC<any> = () => {
  // const [movies, onChangeMovies] = useState<Movie[] | null>(null)
  const navigation = useNavigation<any>()
  const favoriteMovies = useSelector((state: RootState) => state.movies.favoriteMovies)
  console.log('moviesFavoritesScreen', favoriteMovies)
  // Open post
  const openPostHandler = (post: any) => {
    // navigation.navigate('PostScreen', {postId: post.id})
    navigation.navigate('PostScreen', {postId: post.id, postTitle: post.title})
  }
  // Render
  return (
    <View style={styles.container}>
      <FlatList
        data={favoriteMovies}
        keyExtractor={item => item.id.toString()}
        renderItem={({item}) => <Post post={item} onOpen={openPostHandler}/>}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})
