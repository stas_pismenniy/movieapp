import axios from 'axios'
import { BASE_URL } from '@env'

const http = axios.create({
  baseURL: `${BASE_URL}`
})
export default {
  get (url: string) {
    return http.get(url, {
      headers: {
        'Content-Type': 'application/json'
      }
    })
  }
}
