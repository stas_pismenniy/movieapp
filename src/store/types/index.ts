export const ERROR = 'ERROR'
export const GET_MOVIES = 'GET_MOVIES'
export const GET_MOVIE = 'GET_MOVIE'
export const ADD_TO_FAVORITE = 'ADD_TO_FAVORITE'
export const INCREMENT_PAGE = 'INCREMENT_PAGE'

import Movie from '../../interfaces/interfaces'

export interface MoviesState {
  allMovies: Movie[],
  favoriteMovies: Movie[],
  page: any
}
interface GetMoviesAction {
  type: typeof GET_MOVIES,
  payload: Movie[]
}
// One movie action
interface GetMovieAction {
  type: typeof GET_MOVIE,
  payload: {movie: Movie[], postId: Movie[]}
}

// Add to favorite
interface AddToFavoriteAction {
  type: typeof ADD_TO_FAVORITE,
  payload: number
}

// Add to favorite
interface IncrementPageAction {
  type: typeof INCREMENT_PAGE,
  payload: number
}

export type MoviesActionTypes = GetMoviesAction
export type MovieActionTypes = GetMovieAction
export type AddToFavoriteActionTypes = AddToFavoriteAction
export type IncrementPageActionTypes = IncrementPageAction
