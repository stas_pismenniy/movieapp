import { createStore, compose, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { persistStore, persistReducer } from 'redux-persist'
import AsyncStorage from '@react-native-async-storage/async-storage'
import {rootReducer} from './reducers'

const reducer = rootReducer

const persistConfig = {
  key: 'root',
  storage: AsyncStorage
}

const persistedReducer = persistReducer<any, any>(persistConfig, reducer)
const composeEnhancers = compose

export const store = createStore(
  persistedReducer,
  {},
  composeEnhancers(
    applyMiddleware(thunk)
  )
)
export const persisStore = persistStore(store)
export default store
