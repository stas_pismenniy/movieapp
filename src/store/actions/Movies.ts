import {
  ADD_TO_FAVORITE,
  AddToFavoriteActionTypes,
  GET_MOVIE,
  GET_MOVIES,
  INCREMENT_PAGE,
  IncrementPageActionTypes,
  MovieActionTypes,
  MoviesActionTypes
} from '../types'
import {ActionCreator} from 'redux'

import Movie from '../../interfaces/interfaces'
import api from '../../services/api'
import {API_KEY} from '@env'
// Get movies list
const getMoviesSuccess: ActionCreator<MoviesActionTypes> = (movies: Movie[]) => {
  return { type: GET_MOVIES, payload: movies }
}
// Get movies list
export const getMovies = (page: number) => async (dispatch: any, getState: any): Promise <void> => {
  // const increment = page + 1
  console.log('getStateMovie', getState())
  console.log('page123', page)
  // console.log('page123+++', page + 1)
  try {
    api.get(`movie/popular?api_key=${API_KEY}&language=en-US&page=${page}`)
      .then(async response => {
        console.log('response12345', response)
        await dispatch(incrementPage(response.data.page + 1))
        // if (page !== increment) {
        await dispatch(getMoviesSuccess(response.data.results))
        // }
        // dispatch(getMovies(response.data.results))
      })
      .catch(e => console.log('error', e))
  } catch (e) {
    console.log('error', e)
  }
  // dispatch(getMoviesSuccess(movies))
}
// Get movie details
const getMovieSuccess: ActionCreator<MovieActionTypes> = (movie: Movie[], postId: Movie[]) => {
  return { type: GET_MOVIE, payload: {movie: movie, postId: postId} }
}
// Get movie details
export const getMovie = (movie: Record<string, any>, postId: string) => async (dispatch: any): Promise <void> => {
  console.log('MoviegetMovie', movie)
  dispatch(getMovieSuccess(movie, postId))
}

// Add to favorites
const addToFavoritesSuccess: ActionCreator<AddToFavoriteActionTypes> = (postId: number) => {
  return { type: ADD_TO_FAVORITE, payload: postId }
}
// Get movie details
export const addToFavorites = (postId: number) => async (dispatch: any): Promise <void> => {
  console.log('addToFavorites Action', postId)
  dispatch(addToFavoritesSuccess(postId))
}

// INCREMENT_PAGE
const incrementPageSuccess: ActionCreator<IncrementPageActionTypes> = (page: number) => {
  return { type: INCREMENT_PAGE, payload: page }
}
// Get movie details
export const incrementPage = (page: number) => async (dispatch: any): Promise <void> => {
  dispatch(incrementPageSuccess(page))
}
