import {
  GET_MOVIE,
  GET_MOVIES,
  ADD_TO_FAVORITE,
  INCREMENT_PAGE,
  MoviesActionTypes,
  MovieActionTypes,
  AddToFavoriteActionTypes,
  IncrementPageActionTypes,
  MoviesState
}
from '../types/'

const initialState: MoviesState = {
  allMovies: [],
  favoriteMovies: [],
  page: 1
}

export default (
  state: MoviesState = initialState, action: MoviesActionTypes | MovieActionTypes | AddToFavoriteActionTypes | IncrementPageActionTypes):
  MoviesState => {
  console.log('ACTIONState', state)
  console.log('ACTIONPayload', action.payload)
  console.log('ACTION', action)
  switch (action.type) {
    case GET_MOVIES:
      // console.log('equals', equals)
      console.log('state.allMovies', state.allMovies)
      return <MoviesState>{
        ...state,
        allMovies: [...state.allMovies, ...action.payload]
        // allMovies: [...action.payload]
      }
    // const equals = state.allMovies.length === action.payload.length && state.allMovies.every((e, i) => e.id === action.payload[i].id)
    // if (equals) {
    //   return <MoviesState>{
    //     ...state,
    //     allMovies: action.payload
    //   }
    // } else {
    //   return <MoviesState>{
    //     ...state,
    //     allMovies: [...state.allMovies, ...action.payload]
    //   }
    // }
    case GET_MOVIE:
      const allMovies = state.allMovies.map(movie => {
        if (movie.id === action.payload.postId) {
          movie.videos = action.payload.movie.videos.results
          movie.cast = action.payload.movie.credits.cast
        }
        return movie
      })
      return <MoviesState>{
        ...state,
        allMovies
      }
    case ADD_TO_FAVORITE:
      state.allMovies.map(movie => {
        if (movie.id === action.payload) {
          movie.favorite = !movie.favorite
        }
        return movie
      })
      return {
        ...state,
        favoriteMovies: state.allMovies.filter(movie => movie.favorite)
      }
    case INCREMENT_PAGE:
      return {
        ...state,
        page: action.payload
      }
    default:
      return state
  }
}
