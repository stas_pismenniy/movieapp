declare module '@env' {
  export const BASE_URL: string
  export const API_KEY: string
  export const IMAGES_URI: string
}
