import React, {FC} from 'react'
import {StyleSheet, View, Text} from 'react-native'

export const Navbar: FC<any> = () => {
  return (
    <View>
      <Text style={styles.title}>Header Title</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  title: {
    textAlign: 'center'
  }
})
