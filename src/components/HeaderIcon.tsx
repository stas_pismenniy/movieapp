import React, {FC} from 'react'
import {StyleSheet, TouchableOpacity, Image} from 'react-native'
import images from '../utils/images.util'
import { addToFavorites } from '../store/actions/'
import { useDispatch, useSelector } from 'react-redux'
import {useNavigation, useRoute} from '@react-navigation/native'
import {RootState} from '../store/reducers'

export const HeaderIcon: FC<any> = () => {
  const dispatch = useDispatch()
  // const navigation = useNavigation<any>()
  const route: any = useRoute()
  const postId = route.params.postId
  const favoriteMovies = useSelector((state: RootState) => state.movies.favoriteMovies)
  const favoriteMovie = favoriteMovies.find(p => p.id === postId)
  console.log('routeHeaderIcon', route)
  console.log('routeHeaderIconfavoriteMovie', favoriteMovie)
  return (
    <TouchableOpacity onPress={() => {
      // console.log('pressed')
      dispatch(addToFavorites(postId))
    }}>
      {
        favoriteMovie ?
        <Image source={images.favoriteActive} style={styles.icon} /> :
        <Image source={images.favoriteInActive} style={styles.icon} />
      }
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  title: {
    textAlign: 'center'
  },
  icon: {
    width: 20,
    height: 20,
    marginRight: 15
  }
})
