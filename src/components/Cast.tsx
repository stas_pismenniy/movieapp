import React, {FC} from 'react'
import {StyleSheet, View, Text} from 'react-native'

export const Cast: FC<any> = ({title}) => {
  return (
    <View>
      <Text style={styles.title}>{title}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  title: {
    // textAlign: 'center',
    marginBottom: 10,
    fontWeight: '400',
    fontSize: 16
  }
})
