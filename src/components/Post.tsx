import React, {FC} from 'react'
import {View, ImageBackground, Text, StyleSheet, TouchableOpacity} from 'react-native'
import { IMAGES_URI } from '@env'
export const Post: FC<any> = ({post, onOpen}) => {
  return (
    <TouchableOpacity activeOpacity={0.5} style={styles.post} onPress={() => onOpen(post)}>
      <ImageBackground resizeMode="contain" style={styles.image} source={{uri: `${IMAGES_URI}${post.poster_path}`, cache: 'force-cache'}}>
        <View style={styles.textWrap}>
          <Text style={styles.title}>{post.title}</Text>
        </View>
      </ImageBackground>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  post: {
    marginBottom: 20
    // overflow: 'hidden'
  },
  image: {
    width: '100%',
    height: 400,
    justifyContent: 'flex-end'
  },
  textWrap: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    paddingVertical: 5,
    alignItems: 'center',
    width: '100%'
  },
  title: {
    color: '#FFFFFF',
    fontSize: 19,
    fontWeight: 'bold'
  }
})
