import React, {FC} from 'react'
import {
  createMaterialBottomTabNavigator
} from '@react-navigation/material-bottom-tabs'
import { MainScreen } from '../screens/MainScreen'
import { Favorites } from '../screens/Favorites'
const Tab = createMaterialBottomTabNavigator()

const barStyle = {
  backgroundColor: '#ffffff'
}
const BottomTabs: FC<any> = () => {
  return (
    <Tab.Navigator barStyle={barStyle}>
      <Tab.Screen
        name="MainScreen"
        component={MainScreen}
      />
      <Tab.Screen
        name="Favorites"
        component={Favorites}
      />
    </Tab.Navigator>
  )
}

export default BottomTabs
