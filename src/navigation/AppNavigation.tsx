// import React, { useState, useEffect, FC} from 'react'
import React, {FC} from 'react'
import {createStackNavigator} from '@react-navigation/stack'
// import { Splash } from '../screens/Splash'
import MainTabs from './MainTabs'

const Stack = createStackNavigator()
const AppNavigator: FC<any> = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="MainTabs" component={MainTabs} />
    </Stack.Navigator>
  )
}

export default AppNavigator
