import React, {FC} from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { getFocusedRouteNameFromRoute } from '@react-navigation/native'
import {Button} from 'react-native'
import BottomTabs from './Bottomtabs'
import { PostScreen } from '../screens/PostScreen'
import { HeaderIcon } from '../components/HeaderIcon'
// Get header title
function getHeaderTitle(route: any) {
  const routeName = getFocusedRouteNameFromRoute(route) ?? 'Movies'
  switch (routeName) {
    case 'MainScreen':
      return 'Movies'
    case 'Favorites':
      return 'Favorites'
  }
}
const Stack = createStackNavigator()
const options = ({ route }: {route: any}) => ({
  headerTitle: getHeaderTitle(route)
  // title: route?.params?.postTitle
})
const postScreenOptions = ({ route }: {route: any}) => ({
  // headerTitle: (props: any) => <Navbar {...props} />,
  headerRight: () => (
    <HeaderIcon />
  ),
  title: route?.params?.postTitle
})
const MainTabs: FC<any> = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Movies"
        component={BottomTabs}
        options={options}
      />
      <Stack.Screen
        name="PostScreen"
        component={PostScreen}
        options={postScreenOptions}
      />
    </Stack.Navigator>
  )
}

export default MainTabs
