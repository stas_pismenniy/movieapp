interface Movie {
  id: number,
  title?: string,
  image?: any,
  poster_path?: any,
  backdrop_path?: any,
  overview?: string,
  postId?: number,
  videos?: any,
  cast?: any,
  favorite?: any
}
export default Movie
