import React, {FC} from 'react'
import {NavigationContainer} from '@react-navigation/native'
import AppNavigator from './src/navigation/AppNavigation'
import { Provider } from 'react-redux'
import { persisStore, store } from './src/store'
import { PersistGate } from 'redux-persist/integration/react'
import {
  StyleSheet
} from 'react-native'
import {BASE_URL} from '@env'

console.log('Base url', BASE_URL)
const App: FC<any> = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persisStore}>
        <NavigationContainer>
          <AppNavigator/>
        </NavigationContainer>
      </PersistGate>
    </Provider>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})
export default App
